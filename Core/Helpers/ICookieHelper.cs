﻿using System.Net;

namespace Core.Helpers
{
    public interface ICookieHelper
    {
        CookieCollection GetAllCookiesFromHeader(string cookiesHeader, string host);
    }
}
