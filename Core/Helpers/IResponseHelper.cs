﻿using System.Collections.Generic;
using System.Net;

namespace Core.Helpers
{
    public interface IResponseHelper
    {
        bool DoesContentContainValues(HttpWebResponse response, IEnumerable<string> values, IEnumerable<string> notValues = null);

        string GetResponseContent(HttpWebResponse response);
    }
}
