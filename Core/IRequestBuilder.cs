﻿using System.Net;
using WiwOne.Model;

namespace Core
{
    public interface IRequestBuilder
    {
        HttpWebResponse ExecuteGetHttpWebRequest(WiwRequestContext context, bool closeResponse = true);
    }
}
