﻿using WiwOne.Model;

namespace Core.Services
{
    public interface IShopService
    {
        void Enter(WiwRequestContext context);

        void BuyHealthPotion(WiwRequestContext context, int potionCounts);

        void MoveToPointOfSalePotionSection(WiwRequestContext context);

        void MoveToHealthPotionSection(WiwRequestContext context);

        void MoveToPotionsSection(WiwRequestContext context);
    }
}
