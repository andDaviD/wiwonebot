﻿using WiwOne.Model;

namespace Core.Services
{
    public interface IArenaService
    {
        void Enter(WiwRequestContext context);
    }
}
