﻿using WiwOne.Model;

namespace Core.Services
{
    public interface IStruggleService
    {
        void StartFightAgainstSpirit(WiwRequestContext context, FightType fightType);

        void QuiteStruggle(WiwRequestContext context);

        void ReturnToArena(WiwRequestContext context);

        void StruggleAgainstSpirit(WiwRequestContext context);

        void StartDuel(WiwRequestContext context);

        void RequestDuel(WiwRequestContext mainContext, WiwRequestContext enemyContext);

        void SubmitDuelRequest(WiwRequestContext context);
    }
}
