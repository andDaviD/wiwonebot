﻿using WiwOne.Model;

namespace Core.Services
{
    public interface ITavernaService
    {
        void Enter(WiwRequestContext context);

        bool Drink(WiwRequestContext context);

        void Quit(WiwRequestContext context);
    }
}
