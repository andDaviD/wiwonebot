﻿using WiwOne.Model;

namespace Core.Services
{
    public interface IMainHallService
    {
        void Enter(WiwRequestContext context);
    }
}
