﻿using WiwOne.Model;

namespace Core.Services
{
    public interface IAuthorizeService
    {
        void Login(WiwRequestContext context);
    }
}