﻿using WiwOne.Model;

namespace Core.Services
{
    public interface IEntranceService
    {
         void OpenEntrance(WiwRequestContext context);
    }
}
