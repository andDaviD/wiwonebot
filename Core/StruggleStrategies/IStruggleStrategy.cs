﻿using WiwOne.Model;

namespace Core.StruggleStrategies
{
    public interface IStruggleStrategy
    {
        void StartFight(WiwRequestContext context);
    }
}
