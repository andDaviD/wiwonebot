﻿using WiwOne.Model.StruggleStrategies;

namespace Core.StruggleStrategies
{
    public interface IStruggleFactory
    {
        IStruggleStrategy CreateStruggleStrategy(StruggleType struggleType);
    }
}
