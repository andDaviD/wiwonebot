﻿using Core;
using Core.Helpers;
using Core.Services;
using Core.StruggleStrategies;
using Microsoft.Practices.Unity;
using WiwOne.Bot;
using WiwOne.Bot.Helper;
using WiwOne.Model.StruggleStrategies;
using WiwOneBot.Services;
using WiwOneBot.StruggleStrategies;

namespace WiwOneBot
{
    public class DefaultUnityContainer
    {
        private static UnityContainer _instance;

        private DefaultUnityContainer()
        {
        }

        public static UnityContainer Instance
        {
            get { return _instance ?? (_instance = new UnityContainer()); }
        }

        public static void Bootstrap()
        {
            // Requests
            Instance.RegisterType<IRequestBuilder, RequestExecutor>();

            // Helpers
            Instance.RegisterType<ICookieHelper, CookieHelper>();
            Instance.RegisterType<IResponseHelper, ResponseHelper>();

            //Services
            Instance.RegisterType<IAuthorizeService, AuthorizeService>();
            Instance.RegisterType<IArenaService, ArenaService>();
            Instance.RegisterType<IEntranceService, EntranceService>();
            Instance.RegisterType<IMainHallService, MainHallService>();
            Instance.RegisterType<IShopService, ShopService>();
            Instance.RegisterType<IStruggleService, StruggleService>();
            Instance.RegisterType<ITavernaService, TavernaService>();
            Instance.RegisterType<IWiwServiceBase, WiwServiceBase>();

            //Factories
            Instance.RegisterType<IStruggleStrategy, DuelStrategy>(StruggleType.Duel.GetStringValue());
            Instance.RegisterType<IStruggleStrategy, SpiritStrategy>(StruggleType.Spirit.GetStringValue());

            var injectedStrategies = new InjectionConstructor(Instance.ResolveAll<IStruggleStrategy>());
            Instance.RegisterType<IStruggleFactory, StruggleFactory>(injectedStrategies);
        }
    }
}
