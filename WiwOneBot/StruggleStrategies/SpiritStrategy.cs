﻿using System;
using Core;
using Core.StruggleStrategies;
using WiwOne.Model.StruggleStrategies;
using System.Threading;
using WiwOne.Bot;
using Core.Services;
using WiwOne.Model;

namespace WiwOneBot.StruggleStrategies
{
    [StruggleType(StruggleType.Spirit)]
    public class SpiritStrategy : StruggleStrategyBase, IStruggleStrategy
    {
        public SpiritStrategy(
            IEntranceService entranceService, 
            ITavernaService tavernaService, 
            IMainHallService mainHallService,
            IShopService shopService,
            IStruggleService struggleService,
            IArenaService arenaService,
            IAuthorizeService authorizeService) : 
            base(entranceService,
                tavernaService,
                mainHallService,
                shopService,
                struggleService,
                arenaService,
                authorizeService)
        {
        }
        
        public void StartFight(WiwRequestContext context)
        {
            LoginAndEnterMainHall(context);

            for (int i = 0; i < WiwSettings.Default.FightsNumber; i++)
            {
                Console.WriteLine("Fight #{0} is begining...", i);
                
                _tavernaService.Enter(context);
                var isHealingFailed = _tavernaService.Drink(context);
                if (isHealingFailed)
                {
                    BuyHealthPotion(context);
                    Console.WriteLine("Health Potion was bought!");
                    return;
                }
                else
                {
                    _tavernaService.Quit(context);
                    _mainHallService.Enter(context);
                }

                Struggle(context);

                Thread.Sleep(WiwSettings.Default.FightsInterval);
            }
        }

        private void Struggle(WiwRequestContext context)
        {
            _arenaService.Enter(context);
            _struggleService.StartFightAgainstSpirit(context, WiwSettings.Default.SpiritFightType);
            _struggleService.StruggleAgainstSpirit(context);
            _struggleService.ReturnToArena(context);
            _struggleService.QuiteStruggle(context);
            _mainHallService.Enter(context);
        }
    }
}
