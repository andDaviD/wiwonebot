﻿using Core.StruggleStrategies;
using System.Collections.Generic;
using System.Linq;
using WiwOne.Model.StruggleStrategies;

namespace WiwOneBot.StruggleStrategies
{
    public class StruggleFactory : IStruggleFactory
    {
        private IEnumerable<IStruggleStrategy> _struggleStrategies;

        public StruggleFactory(IEnumerable<IStruggleStrategy> struggleStrategies)
        {
            _struggleStrategies = struggleStrategies;
        }

        public IStruggleStrategy CreateStruggleStrategy(StruggleType struggleType)
        {
            return _struggleStrategies.Single(
                strategy => strategy.GetType().GetCustomAttribute<StruggleTypeAttribute>().StruggleType == struggleType);
        }
    }
}
