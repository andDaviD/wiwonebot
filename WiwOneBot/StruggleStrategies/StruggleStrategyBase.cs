﻿using Core;
using Core.Services;
using Core.StruggleStrategies;
using WiwOne.Bot;
using WiwOne.Model;

namespace WiwOneBot.StruggleStrategies
{
    public class StruggleStrategyBase : IStruggleStrategyBase
    {
        protected IEntranceService _entranceService;

        protected ITavernaService _tavernaService;

        protected IMainHallService _mainHallService;

        protected IShopService _shopService;

        protected IStruggleService _struggleService;

        protected IArenaService _arenaService;

        protected IAuthorizeService _authorizeService;

        public StruggleStrategyBase(
            IEntranceService entranceService,
            ITavernaService tavernaService,
            IMainHallService mainHallService,
            IShopService shopService,
            IStruggleService struggleService,
            IArenaService arenaService,
            IAuthorizeService authorizeService)
        {
            _entranceService = entranceService;
            _tavernaService = tavernaService;
            _mainHallService = mainHallService;
            _shopService = shopService;
            _struggleService = struggleService;
            _arenaService = arenaService;
            _authorizeService = authorizeService;
        }

        protected void LoginAndEnterMainHall(WiwRequestContext context)
        {
            _authorizeService.Login(context);
            _entranceService.OpenEntrance(context);
            _mainHallService.Enter(context);
        }

        protected void BuyHealthPotion(WiwRequestContext context)
        {
            _tavernaService.Quit(context);
            _mainHallService.Enter(context);
            _shopService.Enter(context);
            _shopService.MoveToPotionsSection(context);
            _shopService.MoveToHealthPotionSection(context);
            _shopService.MoveToPointOfSalePotionSection(context);
            _shopService.BuyHealthPotion(context, WiwSettings.Default.HealthPotionsCount);
            _shopService.MoveToHealthPotionSection(context);
            _mainHallService.Enter(context);
        }
    }
}
