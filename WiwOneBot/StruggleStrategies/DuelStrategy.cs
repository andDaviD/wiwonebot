﻿using System;
using Core.StruggleStrategies;
using WiwOne.Model.StruggleStrategies;
using Core;
using Core.Services;
using WiwOne.Model;
using System.Net;
using System.Threading;

namespace WiwOneBot.StruggleStrategies
{
    [StruggleType(StruggleType.Duel)]
    public class DuelStrategy : StruggleStrategyBase, IStruggleStrategy
    {
        public DuelStrategy(
            IEntranceService entranceService,
            ITavernaService tavernaService,
            IMainHallService mainHallService,
            IShopService shopService,
            IStruggleService struggleService,
            IArenaService arenaService,
            IAuthorizeService authorizeService) :
            base(entranceService,
                tavernaService,
                mainHallService,
                shopService,
                struggleService,
                arenaService,
                authorizeService)
        {
        }
        
        public void StartFight(WiwRequestContext mainContext)
        {
            LoginAndEnterMainHall(mainContext);

            var proxy = new WebProxy("94.136.95.150:8080");
            proxy.BypassProxyOnLocal = false;
            proxy.UseDefaultCredentials = true;

            var additionalContext = new WiwRequestContext
                                        {
                                            Login = "Galadriel",
                                            Password = "K32FBUQN",
                                            WebProxy = proxy
                                        };

            LoginAndEnterMainHall(additionalContext);

            for (int i = 0; i < WiwSettings.Default.FightsNumber; i++)
            {
                Console.WriteLine("Fight #{0} is begining...", i);

                _tavernaService.Enter(mainContext);
                var isHealingFailed = _tavernaService.Drink(mainContext);
                if (isHealingFailed)
                {
                    BuyHealthPotion(mainContext);
                    Console.WriteLine("Health Potion was bought!");
                    return;
                }
                else
                {
                    _tavernaService.Quit(mainContext);
                    _mainHallService.Enter(mainContext);
                }

                Struggle(mainContext, additionalContext);

                break;

                Thread.Sleep(WiwSettings.Default.FightsInterval);
            }
        }

        private void Struggle(WiwRequestContext mainContext, WiwRequestContext additionalContext)
        {
            _arenaService.Enter(mainContext);
            _struggleService.StartDuel(mainContext);

            _arenaService.Enter(additionalContext);
            _struggleService.RequestDuel(additionalContext, mainContext);
            _struggleService.SubmitDuelRequest(mainContext);

            return;

            _struggleService.ReturnToArena(mainContext);
            _struggleService.QuiteStruggle(mainContext);
            _mainHallService.Enter(mainContext);
        }
    }
}
