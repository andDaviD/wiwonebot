﻿using System;
using System.Linq;

namespace WiwOneBot
{
    public static class TypeExtention
    {
        public static T GetCustomAttribute<T>(this Type type) where T : Attribute
        {
            object[] attributes = type.GetCustomAttributes(false);
            return attributes.OfType<T>().FirstOrDefault();
        }
    }
}
