﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Web;
using Core;
using Core.Helpers;

namespace WiwOne.Bot.Helper
{
    public class CookieHelper : ICookieHelper
    {
        public CookieCollection GetAllCookiesFromHeader(string cookiesHeader, string host)
        {
            var cookieCollection = new CookieCollection();
            if (!string.IsNullOrEmpty(cookiesHeader))
            { 
                List<string> cookieList = ConvertCookieHeaderToList(cookiesHeader);
                cookieCollection = ConvertCookieArraysToCookieCollection(cookieList, host);
            }

            return cookieCollection;
        }

        private List<string> ConvertCookieHeaderToList(string strCookHeader)
        {
            strCookHeader = strCookHeader.Replace("\r", "");
            strCookHeader = strCookHeader.Replace("\n", "");
            string[] strCookTemp = strCookHeader.Split(',');

            var cookieList = new List<string>();
            int i = 0;
            int n = strCookTemp.Length;
            while (i < n)
            {
                if (strCookTemp[i].IndexOf("expires=", StringComparison.OrdinalIgnoreCase) > 0)
                {
                    cookieList.Add(strCookTemp[i] + "," + strCookTemp[i + 1]);
                    i = i + 1;
                }
                else
                {
                    cookieList.Add(strCookTemp[i]);
                }

                i = i + 1;
            }
            return cookieList;
        }
        
        private CookieCollection ConvertCookieArraysToCookieCollection(List<string> cookieList, string strHost)
        {
            var cookieCollection = new CookieCollection();
            int cookieListCount = cookieList.Count;
            for (int i = 0; i < cookieListCount; i++)
            {
                string strEachCook = cookieList[i];
                string[] strEachCookParts = strEachCook.Split(';');
                int intEachCookPartsCount = strEachCookParts.Length;

                var cookTemp = new Cookie();
                for (int j = 0; j < intEachCookPartsCount; j++)
                {
                    if (j == 0)
                    {
                        string strCNameAndCValue = strEachCookParts[j];
                        if (strCNameAndCValue != string.Empty)
                        {
                            int firstEqual = strCNameAndCValue.IndexOf("=", StringComparison.InvariantCultureIgnoreCase);
                            string firstName = strCNameAndCValue.Substring(0, firstEqual);
                            string allValue = strCNameAndCValue.Substring(firstEqual + 1,
                                                                          strCNameAndCValue.Length - (firstEqual + 1));
                            cookTemp.Name = firstName;
                            Encoding iso = Encoding.GetEncoding("utf-8");
                            allValue = HttpUtility.UrlEncode(allValue, iso);
                            cookTemp.Value = allValue;
                        }

                        continue;
                    }

                    string strPNameAndPValue;
                    string[] nameValuePairTemp;
                    if (strEachCookParts[j].IndexOf("path", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        strPNameAndPValue = strEachCookParts[j];
                        if (strPNameAndPValue != string.Empty)
                        {
                            nameValuePairTemp = strPNameAndPValue.Split('=');
                            cookTemp.Path = nameValuePairTemp[1] != string.Empty ? nameValuePairTemp[1] : "/";
                        }

                        continue;
                    }

                    if (strEachCookParts[j].IndexOf("domain", StringComparison.OrdinalIgnoreCase) < 0)
                    {
                        continue;
                    }

                    strPNameAndPValue = strEachCookParts[j];

                    if (string.IsNullOrEmpty(strPNameAndPValue))
                    {
                        continue;
                    }

                    nameValuePairTemp = strPNameAndPValue.Split('=');
                    cookTemp.Domain = nameValuePairTemp[1] != string.Empty ? nameValuePairTemp[1] : strHost;
                }

                if (string.IsNullOrEmpty(cookTemp.Path))
                {
                    cookTemp.Path = "/";
                }

                if (string.IsNullOrEmpty(cookTemp.Domain))
                {
                    cookTemp.Domain = strHost;
                }

                cookieCollection.Add(cookTemp);
            }

            return cookieCollection;
        }
    }
}
