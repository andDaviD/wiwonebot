﻿using Core.Helpers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using WiwOneBot;

namespace WiwOne.Bot.Helper
{
    public class ResponseHelper : IResponseHelper
    {
        public bool DoesContentContainValues(HttpWebResponse response, IEnumerable<string> values, IEnumerable<string> notValues = null)
        {
            var content = GetResponseContent(response);
            return !string.IsNullOrEmpty(content) &&
                    values.Any(content.Contains) &&
                    (notValues == null || !notValues.Any(content.Contains));
        }

        public string GetResponseContent(HttpWebResponse response)
        {
            string content;
            using (Stream receiveStream = response.GetResponseStream())
            {
                using (var streamReader = new StreamReader(receiveStream, Encoding.GetEncoding(WiwSettings.Default.Encoding)))
                {
                    content = streamReader.ReadToEnd();
                }
            }

            return content.ToLower();
        }
    }
}
