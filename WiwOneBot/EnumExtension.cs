﻿using System;
using System.Collections;
using System.Reflection;
using WiwOne.Model;

namespace WiwOne.Bot
{
    public static class EnumExtension
    {
        public static Hashtable StringValues = new Hashtable();

        public static string GetStringValue(this Enum value)
        {
            string output = null;
            Type type = value.GetType();

            if (StringValues.ContainsKey(value))
            {
                var stringValueAttribute = StringValues[value] as StringValueAttribute;
                output = stringValueAttribute?.Value;
            }
            else
            {
                FieldInfo fieldInfo = type.GetField(value.ToString());
                var attributes = fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (attributes != null && attributes.Length > 0)
                {
                    StringValues.Add(value, attributes[0]);
                    output = attributes[0].Value;
                }
            }

            return output;
        }
    }
}
