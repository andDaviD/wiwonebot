﻿using System.Net;
using Core;
using System;
using WiwOne.Model;

namespace WiwOneBot
{
    public class RequestExecutor : IRequestBuilder
    {
        private const int _timeOut = 6 * 10 * 1000; // 1 min

        public HttpWebResponse ExecuteGetHttpWebRequest(WiwRequestContext context, bool closeResponse = true)
        {
            var request = (HttpWebRequest)WebRequest.Create(context.Uri);
            request.Proxy = context.WebProxy;
            request.Method = WebRequestMethods.Http.Get;
            request.KeepAlive = true;
            request.Timeout = _timeOut;
            request.Referer = context.Referer;
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            request.Headers.Add("Upgrade-Insecure-Requests", "1");
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate, sdch");
            request.Headers.Add(HttpRequestHeader.AcceptLanguage, "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            request.CookieContainer = new CookieContainer();
            request.CookieContainer.Add(context.CookieCollection);

            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                context.Referer = context.Uri;
            }
            finally
            {
                if (closeResponse)
                {
                    response?.Close();
                }
            }
            

            return response;
        }
    }
}
