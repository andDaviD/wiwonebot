﻿using Core;
using Microsoft.Practices.Unity;
using Core.StruggleStrategies;
using WiwOneBot.StruggleStrategies;
using WiwOne.Model.StruggleStrategies;
using Core.Services;
using WiwOneBot.Services;
using Core.Helpers;
using WiwOne.Bot.Helper;
using WiwOne.Bot;
using WiwOne.Model;
using System.Net;

namespace WiwOneBot
{
    class Program
    {
        static void Main(string[] args)
        {
            DefaultUnityContainer.Bootstrap();        

            var context = new WiwRequestContext
                              {
                                  Login = WiwSettings.Default.Login,
                                  Password = WiwSettings.Default.Password,
                                  IsMainCharacter = true,
                              }; 

            var struggleFactory = DefaultUnityContainer.Instance.Resolve<IStruggleFactory>();
            var struggleStrategy = struggleFactory.CreateStruggleStrategy(WiwSettings.Default.StruggleStrategy);
            struggleStrategy.StartFight(context);                
        }
    }
}
