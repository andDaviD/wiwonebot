﻿using Core;
using Core.Helpers;
using Core.Services;
using System.Net;
using WiwOne.Model;

namespace WiwOneBot.Services
{
    public class TavernaService : WiwServiceBase, ITavernaService
    {
        public TavernaService(IResponseHelper responseHelper, IRequestBuilder requestBuilder) 
            : base(responseHelper, requestBuilder)
        {
        }

        public void Enter(WiwRequestContext context)
        {
            context.Uri = baseServiceAddress + $"tavern.cgi?sid={context.Sid}&action=enter";
            context.Referer = baseServiceAddress + $"tavern.cgi?sid={context.Sid}";
            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }

        public bool Drink(WiwRequestContext context)
        {
            context.Uri = baseServiceAddress + $"tavern.cgi?action=drink&sid={context.Sid}&id1=combi1&id4=combi4";
            context.Referer = baseServiceAddress + $"tavern.cgi?sid={context.Sid}&action=enter";

            var response = _requestBuilder.ExecuteGetHttpWebRequest(context, false);
            var result =  IsCharacterInjured(response);
            response.Close();

            return result;
        }

        public void Quit(WiwRequestContext context)
        {
            context.Uri = baseServiceAddress + $"tavern.cgi?sid={context.Sid}&action=quit";
            context.Referer = baseServiceAddress + $"place.cgi?sid={context.Sid}";

            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }

        private bool IsCharacterInjured(HttpWebResponse response)
        {
            var injuredValues = new[] { "травма" };
            var healedValues = new[] { "вылечились" };
            return _responseHelper.DoesContentContainValues(response, injuredValues, healedValues);
        }
    }
}
