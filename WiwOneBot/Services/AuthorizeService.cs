﻿using Core.Helpers;
using Core.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Web.Script.Serialization;
using WiwOne.Model;

namespace WiwOneBot.Services
{
    public class AuthorizeService : IAuthorizeService
    {
        private const int _timeOut = 6 * 10 * 1000; // 1 min

        private readonly ICookieHelper _cookieHelper;

        public AuthorizeService(ICookieHelper cookieHelper)
        {
            _cookieHelper = cookieHelper;
        }

        public void Login(WiwRequestContext context)
        {
            HttpWebResponse response = null;
            string jsonResponse, cookies;

            try
            {
                response = Authenticate(context);
                cookies = response.GetResponseHeader("Set-Cookie");
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    jsonResponse = sr.ReadToEnd();
                }
            }
            finally
            {
                response?.Close();
            }

            try
            {
                response = Authorize(cookies, jsonResponse, context);
                context.CookieCollection = response.Cookies;
            }
            finally
            {
                response?.Close();
            }
        }

        private HttpWebResponse Authenticate(WiwRequestContext context)
        {
            string content = $"action=login&pre=1&js=1&json=1&nickname={context.Login}&password={context.Password}";
            
            var request = CreatePostHttpWebRequest(
                "http://www.wiw1.ru/router", 
                context.WebProxy,
                cachePolicy: null,
                accept: "application/json, text/javascript, */*; q=0.01",
                contentType: "application/x-www-form-urlencoded",
                allowAutoRedirect: true,
                header: new KeyValuePair<string, string>("X-Requested-With", "XMLHttpRequest"));

            WriteToStream(request, content);

            return (HttpWebResponse)request.GetResponse();
        }

        private HttpWebResponse Authorize(string cookiesHeader, string jsonResponse, WiwRequestContext context)
        {
            var request = CreatePostHttpWebRequest(
                "http://ent.wiw1.ru/router", 
                context.WebProxy,
                cachePolicy: new HttpRequestCachePolicy(HttpCacheAgeControl.MaxAge, TimeSpan.Zero),
                accept: "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                contentType: "application/x-www-form-urlencoded",
                allowAutoRedirect: false,
                header: new KeyValuePair<string, string>("Upgrade-Insecure-Requests", "1"));

            var cookieCollection = _cookieHelper.GetAllCookiesFromHeader(cookiesHeader, request.Host);
            request.CookieContainer.Add(cookieCollection);

            //POST BODY
            var serializer = new JavaScriptSerializer();
            var jsonCollection = serializer.Deserialize<Dictionary<string, string>>(jsonResponse);
            var content = "sid=" + jsonCollection["sid"] + "&site=" + jsonCollection["site"];

            WriteToStream(request, content);

            context.Sid = jsonCollection["sid"];

            return (HttpWebResponse)request.GetResponse();
        }
        
        private HttpWebRequest CreatePostHttpWebRequest(
            string requestedUri,
            IWebProxy webProxy, 
            HttpRequestCachePolicy cachePolicy, 
            string accept,
            string contentType,
            bool allowAutoRedirect,
            KeyValuePair<string, string> header)
        {
            ServicePointManager.Expect100Continue = webProxy == null;

            var request = (HttpWebRequest)WebRequest.Create(requestedUri);
            request.Proxy = webProxy;
            request.Method = WebRequestMethods.Http.Post;
            request.KeepAlive = true;
            request.Referer = "http://www.wiw1.ru/";
            request.Accept = accept;
            request.Timeout = _timeOut;
            request.ReadWriteTimeout = _timeOut;
            request.ContinueTimeout = _timeOut;
            request.AllowAutoRedirect = allowAutoRedirect;
            request.ContentType = contentType;
            request.UserAgent =
                  "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36";
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
            request.Headers.Add(HttpRequestHeader.AcceptLanguage, "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            request.Headers.Add(header.Key, header.Value);
            request.CookieContainer = new CookieContainer();

            return request;
        }

        private void WriteToStream(HttpWebRequest request, string content)
        {
            byte[] encodedPostParams = Encoding.ASCII.GetBytes(content);
            request.ContentLength = encodedPostParams.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(encodedPostParams, 0, encodedPostParams.Length);
            }
        }
    }
}
