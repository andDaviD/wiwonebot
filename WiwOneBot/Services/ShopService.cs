﻿using Core.Services;
using Core;
using Core.Helpers;
using WiwOne.Model;

namespace WiwOneBot.Services
{
    public class ShopService : WiwServiceBase, IShopService
    {
        public ShopService(IResponseHelper responseHelper, IRequestBuilder requestBuilder) 
            : base(responseHelper, requestBuilder)
        {
        }

        public void Enter(WiwRequestContext context)
        {
            context.Uri = baseServiceAddress + $"shop.cgi?action=enter&sid={context.Sid}";
            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }

        public void BuyHealthPotion(WiwRequestContext context, int potionCounts)
        {
            context.Uri = baseServiceAddress + $"shop.cgi?action=buyshop&sid={context.Sid}&gid=2590&buycnt={potionCounts}";
            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }

        public void MoveToPointOfSalePotionSection(WiwRequestContext context)
        {
            context.Uri = baseServiceAddress + $"shop.cgi?action=buy&sid={context.Sid}&gid=2590";
            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }

        public void MoveToHealthPotionSection(WiwRequestContext context)
        {
            context.Uri = baseServiceAddress + $"shop.cgi?action=cat&submode=potionH&sid={context.Sid}";
            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }

        public void MoveToPotionsSection(WiwRequestContext context)
        {
            context.Uri = baseServiceAddress + $"shop.cgi?action=cat&submode=potions&sid={context.Sid}";
            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }

    }
}
