﻿using Core;
using Core.Helpers;
using Core.Services;
using WiwOne.Model;

namespace WiwOneBot.Services
{
    public class EntranceService : WiwServiceBase, IEntranceService
    {
        public EntranceService(IResponseHelper responseHelper, IRequestBuilder requestBuilder) 
            : base(responseHelper, requestBuilder)
        {
        }

        public void OpenEntrance(WiwRequestContext context)
        {
            context.Uri = baseServiceAddress + $"entrance.cgi?sid={context.Sid}";
            context.Referer = "http://ent.wiw1.ru/router";
            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }
    }
}
