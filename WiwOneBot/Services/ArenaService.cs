﻿using Core;
using Core.Helpers;
using Core.Services;
using WiwOne.Model;

namespace WiwOneBot.Services
{
    public class ArenaService : WiwServiceBase, IArenaService
    {
        public ArenaService(IResponseHelper responseHelper, IRequestBuilder requestBuilder) 
            : base(responseHelper, requestBuilder)
        {
        }

        public void Enter(WiwRequestContext context)
        {
            context.Uri = baseServiceAddress + $"struggle.cgi?sid={context.Sid}&action=enter&scriptid=4BE2F33F1D81357F51126301F730BDE8";
            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }
    }
}
