﻿using Core;
using Core.Helpers;
using Core.Services;
using WiwOne.Model;

namespace WiwOneBot.Services
{
    public class WiwServiceBase : IWiwServiceBase
    {
        protected const string baseServiceAddress = "http://aradon.wiw1.ru/cgi-bin/";
              
        protected readonly IRequestBuilder _requestBuilder;

        protected readonly IResponseHelper _responseHelper;

        public WiwServiceBase(IResponseHelper responseHelper, IRequestBuilder requestBuilder)
        {
            _responseHelper = responseHelper;
            _requestBuilder = requestBuilder;
        }
    }
}
