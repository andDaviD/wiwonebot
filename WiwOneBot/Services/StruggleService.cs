﻿using System;
using Core;
using Core.Helpers;
using Core.Services;
using WiwOne.Model;
using WiwOne.Bot;
using System.Net;
using System.Threading;

namespace WiwOneBot.Services
{
    public class StruggleService : WiwServiceBase, IStruggleService
    {
        public StruggleService(IResponseHelper responseHelper, IRequestBuilder requestBuilder) 
            : base(responseHelper, requestBuilder)
        {
        }

        public void StartFightAgainstSpirit(WiwRequestContext context, FightType fightType)
        {
            context.Uri = baseServiceAddress + $"struggle.cgi?action=addrequest&sid={context.Sid}&fighttype={fightType.GetStringValue()}";
            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }

        public void QuiteStruggle(WiwRequestContext context)
        {
            context.Uri = baseServiceAddress + $"struggle.cgi?sid={context.Sid}&action=quit";
            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }

        public void ReturnToArena(WiwRequestContext context)
        {
            context.Uri = baseServiceAddress + $"struggle.cgi?action=returnarena&sid={context.Sid}";
            context.Referer = baseServiceAddress + $"place.cgi?sid={context.Sid}";
            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }
        
        public void StruggleAgainstSpirit(WiwRequestContext context)
        {
            const int maxRangeRoundNumber = 10;
            const int minField = 1;
            const int maxField = 4;

            var randomizer = new Random();
            int enemyFieldPosition = 20;
            int round = 1;
            for (; round <= maxRangeRoundNumber; round++, enemyFieldPosition--)
            {
                int move = randomizer.Next(minField, maxField);
                int strike = randomizer.Next(minField, maxField);

                var response = StruggleRange(context, round, move, enemyFieldPosition, strike);
                if (IsButtleFinished(response))
                {
                    return;
                }

                Thread.Sleep(WiwSettings.Default.MoveInterval);
            }

            var meleeStrikes = new[] { "h", "b", "p", "f" };
            var meleeMoves = new[] { "hf", "bh", "pb", "fp" };
            for (; ; round++)
            {
                int strikeNumber = randomizer.Next(0, meleeStrikes.Length);
                string strike = meleeStrikes[strikeNumber];

                int moveNumber = randomizer.Next(0, meleeMoves.Length);
                string move = meleeMoves[moveNumber];

                var response = StruggleMelee(context, round, strike, move);
                if (IsButtleFinished(response))
                {
                    return;
                }

                Thread.Sleep(WiwSettings.Default.MoveInterval);
            }
        }

        private HttpWebResponse StruggleRange(WiwRequestContext context, int round, int move, int enemyFieldPosition, int strike)
        {
            context.Uri = string.Format(
                "http://aradon.wiw1.ru/cgi-bin/struggle.cgi?move={1}%5F{2}&sid={0}&striketarget=0&showanim=on&scroll1=&strike={3}%5F{4}&scroll0=&showlog=short&action=move&scroll2=&round={1}",
                context.Sid, round, move, enemyFieldPosition, strike);

            context.Referer = baseServiceAddress + $"place.cgi?sid={context.Sid}";
            return _requestBuilder.ExecuteGetHttpWebRequest(context, false);
        }

        private HttpWebResponse StruggleMelee(WiwRequestContext context, int round, string strike, string move)
        {
            context.Uri = string.Format(
                    "http://aradon.wiw1.ru/cgi-bin/struggle.cgi?strike={2}&move={3}&sid={0}&showanim=on&scroll1=&scroll0=&round={1}&striketarget=0&action=near&showlog=short&scroll2=",
                    context.Sid, round, strike, move);

            context.Referer = baseServiceAddress + $"place.cgi?sid={context.Sid}";
            return _requestBuilder.ExecuteGetHttpWebRequest(context, false);
        }

        private bool IsButtleFinished(HttpWebResponse response)
        {
            var values = new[] { "oкончен", "повержен", "ничья", "проигрыш" };
            return _responseHelper.DoesContentContainValues(response, values);
        }

        public void StartDuel(WiwRequestContext context)
        {
            context.Uri = baseServiceAddress + $"struggle.cgi?action=addrequest&sid={context.Sid}&fighttype=fight&fNoLim=0&fNoGoods=0&fNearOnly=0";
            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }

        public void RequestDuel(WiwRequestContext mainContext, WiwRequestContext enemyContext)
        {
            mainContext.Uri = baseServiceAddress + $"struggle.cgi?action=request&sid={mainContext.Sid}&join=%CF%F0%E8%F1%EE%E5%E4%E8%ED%E8%F2%FC%F1%FF&fid={enemyContext.Sid}";
            mainContext.Referer = baseServiceAddress + $"struggle.cgi?sid={mainContext.Sid}";
            _requestBuilder.ExecuteGetHttpWebRequest(mainContext);
        }

        public void SubmitDuelRequest(WiwRequestContext context)
        {
            context.Uri = baseServiceAddress + $"struggle.cgi?action=request&sid={context.Sid}&submit=%CF%F0%E8%ED%FF%F2%FC";
            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }
    }
}
