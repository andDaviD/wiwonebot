﻿using Core.Services;
using Core;
using Core.Helpers;
using WiwOne.Model;

namespace WiwOneBot.Services
{
    public class MainHallService : WiwServiceBase, IMainHallService
    {
        public MainHallService(IResponseHelper responseHelper, IRequestBuilder requestBuilder)
            : base(responseHelper, requestBuilder)
        {
        }

        public void Enter(WiwRequestContext context)
        {
            context.Uri = baseServiceAddress + $"place.cgi?sid={context.Sid}";
            _requestBuilder.ExecuteGetHttpWebRequest(context);
        }              
    }
}
