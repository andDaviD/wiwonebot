﻿using System;

namespace WiwOne.Model.StruggleStrategies
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class StruggleTypeAttribute : Attribute
    {
        public StruggleTypeAttribute(StruggleType type)
        {
            StruggleType = type;
        }

        public StruggleType StruggleType { get; }
    }
}
