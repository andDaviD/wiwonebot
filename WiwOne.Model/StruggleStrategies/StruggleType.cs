﻿using System;

namespace WiwOne.Model.StruggleStrategies
{
    [Serializable]
    public enum StruggleType
    {
        [StringValue("Duel")]
        Duel,

        [StringValue("Spirit")]
        Spirit        
    }
}
