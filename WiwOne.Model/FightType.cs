﻿
namespace WiwOne.Model
{
    public enum FightType
    {
        [StringValue("ft_spirit_human")]
        Human,

        [StringValue("ft_spirit_elf")]
        Elf,

        [StringValue("ft_spirit_gnome")]
        Dwarf,

        [StringValue("ft_mirror")]
        Mirror
    }
}
