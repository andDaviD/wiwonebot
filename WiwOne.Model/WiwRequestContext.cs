﻿using System.Net;

namespace WiwOne.Model
{
    public class WiwRequestContext
    {
        public bool IsMainCharacter { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public string Sid { get; set; }

        public CookieCollection CookieCollection { get; set; }

        public string Uri { get; set; }

        public string Referer { get; set; }

        public IWebProxy WebProxy { get; set; }
    }
}
